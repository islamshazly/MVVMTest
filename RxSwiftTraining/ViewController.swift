//
//  ViewController.swift
//  RxSwiftTraining
//
//  Created by Islam Elshazly on 7/29/18.
//  Copyright © 2018 Islam Elshazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    var variableRX = Variable("")
    var behavirrSubject = BehaviorSubject(value: 1)
    let dispose = DisposeBag()
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tryVariableRX()
        self.variableRX.value = "another value"
        self.variableRX.value = "try to show the stats of the issue"
        self.variableRX.value = "try to show the stats of the issue implements "
        self.variableRX.value = "another commit  "
        tryBehaviourSubject()
        // Do any additional setup after loading the view, typically from a nib.
    }
}

extension ViewController {
    
    func tryVariableRX() {
        
        variableRX.value = " first value "
        variableRX.asObservable().subscribe(onNext: { (newValue) in
            print(newValue)
        }, onError: { (_) in
            
        }, onCompleted: {
            // code of complereness // never complete
        }) {
            // on disposd
            }.disposed(by: dispose)
        
        variableRX.value = " seconde value "
        variableRX.value = " third value "
        variableRX.value = "try to fix issue"
    }
    
    func tryBehaviourSubject() {
        
        let observer = behavirrSubject.subscribe(onNext: { (newValue) in
            print("new value \(newValue)")
        }, onError: { (error) in
            print(error.localizedDescription)
        }, onCompleted: {
            print(" complete")
        }) {
            print("dispose")
        }
    }
}
