//
//  UserModel.swift
//  RxSwiftTraining
//
//  Created by Islam Elshazly on 8/5/18.
//  Copyright © 2018 Islam Elshazly. All rights reserved.
//

import UIKit

final class UserModel: NSObject {

    var firstName: String = "Islam"
    var lastName: String = "Elshazly"
}
