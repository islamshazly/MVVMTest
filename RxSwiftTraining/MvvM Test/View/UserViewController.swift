//
//  UserViewController.swift
//  RxSwiftTraining
//
//  Created by Islam Elshazly on 8/5/18.
//  Copyright © 2018 Islam Elshazly. All rights reserved.
//

import UIKit

final class UserViewController: UIViewController {
    
    //MARK:- Variables
    
    private var userViewModel: UserViewModel = UserViewModel()
    
    
    //MARK:- outlets
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var firstNameTxf: UITextField!
    @IBOutlet weak var lastNameTxf: UITextField!
    
    
    //MARK:- life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fillUserDate()
    }
    //MARK:- Helper methods
    
    func fillUserDate() {
        userNameLabel.text = userViewModel.userName
        firstNameTxf.placeholder = userViewModel.userFirstName()
        lastNameTxf.placeholder = userViewModel.userLastName()
        firstNameLabel.text = "first Name "
        lastNameLabel.text = "last Name "
    }
    
    func ShowFirstNameLogic() {
        if userViewModel.validateFirstName() == .valid {
            firstNameLabel.text = firstNameTxf.text
            firstNameLabel.textColor = .green
        } else if userViewModel.validateFirstName() == .invalid {
            firstNameLabel.textColor = .red
            firstNameLabel.text = "still invalid first user name, user name should more than 6 cars "
        }
    }
    
    func ShowLastNameLogic() {
        if userViewModel.validateLastName() == .valid {
            lastNameLabel.text = lastNameTxf.text
            lastNameLabel.textColor = .green
        } else if userViewModel.validateLastName() == .invalid {
            lastNameLabel.textColor = .red
            lastNameLabel.text = "still invalid last user name, user name should more than 6 cars "
        }
    }
    
}

//MARK:- textfiled Delegate
extension UserViewController: UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case firstNameTxf:
            userViewModel.updateFirstName(firstName: textField.text!)
            ShowFirstNameLogic()
        case lastNameTxf:
            userViewModel.updateLastName(lastName: textField.text!)
            ShowLastNameLogic()
        default:
            break
        }
        
        return true
    }
    
    
}
