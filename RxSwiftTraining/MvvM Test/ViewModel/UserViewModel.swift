//
//  UserViewModel.swift
//  RxSwiftTraining
//
//  Created by Islam Elshazly on 8/5/18.
//  Copyright © 2018 Islam Elshazly. All rights reserved.
//

import Foundation

enum UserNameState{
    case valid
    case invalid
}

final class UserViewModel {
    
    //MARK:-  constants
    
    private let minFirstNameLenght = 3
    private let minLastNameLenght = 5
    
    //MARK:-  Variables
    private var user: UserModel = UserModel()
    private var userNameState: UserNameState?
    
    var userName: String {
        return user.firstName + " " + user.lastName
    }
}

//MARK:-  Setters
extension UserViewModel {
    
    func updateFirstName(firstName: String) {
            user.firstName = firstName
    }
    
    func updateLastName(lastName: String) {
            user.lastName = lastName
    }
    
    func userFirstName() -> String {
        return user.firstName
    }
    
    func userLastName() -> String {
        return user.lastName
    }
}
//MARK:- Logic
extension UserViewModel {
    func validateFirstName() -> UserNameState {
        
        return user.firstName.count >= minFirstNameLenght ? .valid : .invalid
    }
    
    func validateLastName() -> UserNameState {
        return user.lastName.count >= minLastNameLenght ? .valid : .invalid
    }
}
